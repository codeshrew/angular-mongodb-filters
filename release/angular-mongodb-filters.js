(function(window, angular, undefined) {
  'use strict';

  var angularMongodbFilters = angular.module('angularMongodbFilters', []);

  angularMongodbFilters
  .filter('populateIdFromArray', function() {
    return function(object_id, arrayOfFullObjects, emptyState) {
      try {
        var matchedObject = _.find(arrayOfFullObjects, function(fullObject) {
          return fullObject._id === object_id;
        });

        if (matchedObject) {
          return matchedObject;
        }

        return emptyState ? emptyState : ''; // Return the empty state that was fed in if it was set
      } catch (e) {
        return ''; // return empty string if no matches
      }
    };
  })
  .filter('populateIdFromArrayAndReturnKey', function() {
    return function(object_id, arrayOfFullObjects, keyName, emptyState) {
      try {
        var matchedObject = _.find(arrayOfFullObjects, function(fullObject) {
          return fullObject._id === object_id;
        });

        if (matchedObject) {
          return _.get(matchedObject, keyName);
        }

        return emptyState ? emptyState : ''; // Return the empty state that was fed in if it was set
      } catch (e) {
        return ''; // return empty string if no matches
      }
    };
  });

})(window, window.angular);
