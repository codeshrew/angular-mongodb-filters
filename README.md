# angular-mongodb-filters
AngularJS filters for handling MongoDB related stuff

# Install
`bower install angular-mongodb-filters`

Add `<script src="bower_components/angular-mongodb-filters/release/angular-mongodb-filters.js"></script>` to your site.

Add `angularMongodbFilters` to your app dependencies.

# Dependencies
This project currently depends on lodash, planned for future updates is to remove this dependency, but for now you'll have to include:

`<script src="bower_components/lodash/lodash.min.js"></script>`

# Available Filters
- populateIdFromArrayAndReturnKey
- populateIdFromArray

# populateIdFromArrayAndReturnKey

### Arguments
`{{ <object_id> | populateIdFromArrayAndReturnKey:<arrayOfFullObjects>:<keyName>:<emptyState> }}`

1. `<object_id>` (String): The ObjectID reference as a string.
1. `<arrayOfFullObjects>` (Array): The array of objects which will be searched with the `object_id` above.
1. `<keyName>` (String): The name of the key to pull out of the object found in the `arrayOfFullObjects` above. Value must be wrapped in quotes, e.g. `'name'`.
1. `<emptyState>` (String): *Optional*. Defaults to `''` if not set. Will be displayed if no matches are found in the `arrayOfFullObjects` or no key was found for `keyName`. Value must be wrapped in quotes, e.g. `'Nothing Found'`.

### Usage
With a user that looks like this:

```
{
  "username": "user",
  "role": "1234567" // MongoDB ObjectID reference to a role
}
```

And a role that looks like this:

```
{
  "_id": "1234567",
  "name": "admin"
}
```

If you have the unpopulated user and the array of roles on the front end, you can use this filter to get a key off the appropriate role object by `_id`.
```
{{ user.role | populateIdFromArrayAndReturnKey:roles:'name':'–' }}
```

# populateIdFromArray

### Arguments
`{{ <object_id> | populateIdFromArray:<arrayOfFullObjects>:<emptyState> }}`

1. `<object_id>` (String): The ObjectID reference as a string.
1. `<arrayOfFullObjects>` (Array): The array of objects which will be searched with the `object_id` above.
1. `<emptyState>` (String): *Optional*. Defaults to `''` if not set. Will be displayed if no matches are found in the `arrayOfFullObjects`. Value must be wrapped in quotes, e.g. `'Nothing Found'`.

### Usage
With a user that looks like this:

```
{
  "username": "user",
  "role": "1234567" // MongoDB ObjectID reference to a role
}
```

And a role that looks like this:

```
{
  "_id": "1234567",
  "name": "admin"
}
```

If you have the unpopulated user and the array of roles on the front end, you can use this filter to get back the entire role object by `_id`.
```
{{ user.role | populateIdFromArray:roles:'name':'–' }}
```

# Planned Updates
In no particular order:

- Add tests
- Add example page to demonstrate usage
- Remove lodash dependency
